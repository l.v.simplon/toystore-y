import React, { useState } from 'react';



export default function Toy({id, toy, decrementNb}){
    //manage stock in individual Toys instead of in App?
    //const [stock, setStock]= useState(6)

    const handleClickDecrement = () => {
        //call App's decrementNb function, that was passed as props
        decrementNb(id)
    }

    return <li onClick={handleClickDecrement}>{toy.name} quantité:{toy.nb}</li>
}