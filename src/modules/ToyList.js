import React from 'react';
import Toy from "./Toy";

/*object destructuration: equivalent to
export default function ToyList(props) {
    const toys = props.toys;
    const decrementNb = props.decrementNb;
    ...
*/
export default function ToyList({toys, decrementNb}) {
    return <ul>
        {
            //iterate over toys
            toys.map((toy, index) => 
                <Toy key={index} id={index} toy={toy} decrementNb={decrementNb}/>
            )
        }
    </ul>
}