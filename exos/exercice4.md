Exercice 4 : Les composants parlent aux composants
==================================================

# Ajouter un composant d'alerte lorsqu'un stocke de jouet
* Vert quand tous les jouets ont un stock supérieur à 2
* Jaune quand un jouet est inférieur ou égal à 2
* Rouge quand un jouet a un stock égal à 0
